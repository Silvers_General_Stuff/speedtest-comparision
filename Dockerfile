FROM debian:buster-slim

# install dependencies
COPY install.sh .
RUN ./install.sh

# copy the scripts into it
COPY ./fast_com.sh . 
COPY ./speedtest_net.sh .
COPY ./controller.sh . 

CMD [ "./controller.sh" ] 
