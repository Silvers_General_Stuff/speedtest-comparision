#!/bin/bash

host="fast.com"

# get speedtest
fast --upload > /tmp/speedtest_fast

# Conversions
download=$(head -n 1 /tmp/speedtest_fast)

download=${download/ Gbps/000000000}
download=${download/ Mbps/000000}
download=${download/ Kbps/000}
download=${download/ bps/}

upload=$(tail -1 /tmp/speedtest_fast)
upload=${upload/ Gbps/000000000}
upload=${upload/ Mbps/000000}
upload=${upload/ Kbps/000}
upload=${upload/ bps/}

# send to DB
curl -i -XPOST "${DB_SERVER}/write?db=${db}" --data-binary "speedtest,host=${host} download=${download},upload=${upload}"

# clean up
rm /tmp/speedtest_fast