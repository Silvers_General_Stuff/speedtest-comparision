#!/bin/bash

# foundations
apt update && apt upgrade
apt-get install -yq curl jq

# node.js - for fast.com
curl -sL https://deb.nodesource.com/setup_15.x | bash -
apt-get install -yq nodejs

# fast.com
npm install --global fast-cli
# puppeterr
apt-get install -yq \
          gconf-service libasound2 libatk1.0-0 libatk-bridge2.0-0 libc6 libcairo2 libcups2 libdbus-1-3 \
          libexpat1 libfontconfig1 libgcc1 libgconf-2-4 libgdk-pixbuf2.0-0 libglib2.0-0 libgtk-3-0 libnspr4 \
          libpango-1.0-0 libpangocairo-1.0-0 libstdc++6 libx11-6 libx11-xcb1 libxcb1 libxcomposite1 libxcursor1 \
          libxdamage1 libxext6 libxfixes3 libxi6 libxrandr2 libxrender1 libxss1 libxtst6 ca-certificates \
          fonts-liberation libappindicator1 libnss3 lsb-release xdg-utils wget

# speedtest.net
apt-get install -yq gnupg1 apt-transport-https dirmngr
export INSTALL_KEY=379CE192D401AB61
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys $INSTALL_KEY
echo "deb https://ookla.bintray.com/debian generic main" | tee  /etc/apt/sources.list.d/speedtest.list
apt-get update
apt-get install -yq speedtest

