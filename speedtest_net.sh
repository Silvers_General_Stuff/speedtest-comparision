#!/bin/bash

# get speedtest
result=$(speedtest --accept-license --accept-gdpr -f json)

# split out
download=$(echo $result | jq '.download.bandwidth')
upload=$(echo $result | jq '.upload.bandwidth')

download=$(( download * 8 ))
upload=$(( upload * 8 ))

# send to DB
curl -i -XPOST "${DB_SERVER}/write?db=${db}" --data-binary "speedtest,host=speedtest.net download=${download},upload=${upload}"