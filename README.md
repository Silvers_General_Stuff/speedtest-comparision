# Speedtest tracking and comparision

 This projects uses multiple speedtests
 
 * Fast.com
 * Speedtest.net
 
 Almost all of teh speedtest tracking tools use speedtest.net, however it is known that some ISP's favor that sitre while also blocvking other sites leading to untrue results.  
 Fast.com is hosted from netflix's servers so if netflix is being throttled it shows upo in their speedtest.  
 Ideal state is that both tests give teh same result.
 
 This is a collection of bash scripts that take the tests and dump them into influxDB for use in grafana.
 
 Requirements
 
 * https://github.com/sindresorhus/fast-cli
 * https://www.speedtest.net/apps/cli